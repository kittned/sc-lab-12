package AveragePoint;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Homework {
	FileWriter fileWriter = null;
	String filename = "homework.txt";
	public void CallPoint(){
		try {
			// read from user

			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			// write to file
			FileWriter fileWriter = new FileWriter("average.txt", true);
			PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));
			String line = buffer.readLine();
			int i = 0;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] sum1 = line.split(",");
				double amount = 0.0;
				String name = (sum1[0].trim());

				for(i=1; i<sum1.length; i++){
					double num = Double.parseDouble(sum1[i].trim());
					amount += num;
				}
				out.println(name + " has total Point Homework : " + amount/5);
			}
			out.flush();
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file " + filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}
		finally {
			try {
				FileReader fileReader = new FileReader("average.txt");
				if (fileReader != null)
					fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
	}
}
