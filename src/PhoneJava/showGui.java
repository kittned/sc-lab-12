package PhoneJava;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class showGui extends JFrame {

	private JLabel showProjectName;
	private JTextArea showResults;
	private JButton endButton;
	private String str;

	public showGui() {
		createFrame();
	}

	public void createFrame() {
		this.showProjectName = new JLabel("                                           The PhoneBook System");
		this.showResults = new JTextArea("                                             Show Result");
		this.endButton = new JButton("End this program");
		setLayout(new BorderLayout());
		add(this.showProjectName, BorderLayout.NORTH);
		add(this.showResults, BorderLayout.CENTER);
		add(this.endButton, BorderLayout.SOUTH);
	}

	public void setProjectName(String s) {
		showProjectName.setText(s);
	}

	public void setResult(String str) {
		this.str = str;
		showResults.setText(this.str);
	}

	public void extendResult(String str) {
		this.str = this.str + "\n" + str;
		showResults.setText(this.str);
	}

	public void setListener(ActionListener list) {
		endButton.addActionListener(list);
	}
}
